<?php
use app\models\UploadForm;
use yii\web\UploadedFile;
class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     archivo:
registrar-codigo.php

Te envio:
php_idfb
php_codigo  
php_sucursal
php_caja
php_ticket

Me envias:
resultado=1 ó 0

Archivo:
registrar-pieza.php

Te envio:
php_idfb
php_totalcodigos
php_pieza1
php_pieza2
php_pieza3
php_pieza4
php_pieza5
php_pieza6

Me envias:
resultado=1 ó 0

Archivo:
registrar-terminado.php

Te envio:
php_idfb
php_terminados
php_pieza1
php_pieza2
php_pieza3
php_pieza4
php_pieza5
php_pieza6

Me envias:
resultado=1 ó 0
     */
    public function actionIndex() {
       $this->render('index',array(
         
        ));

    }
        public function actionUcubiertos() {
        $user= UserCubiertos::model()->findbyPk($_POST['identity']);
        echo CJSON::encode($user);

    }
      public function actionCheckCode() {
        if (isset($_POST['php_idfb'])) {
            $return="resultado=0";
            $model= OpdCouponTransaction::model()->findByAttributes(array("coupon_number"=>$_POST["php_codigo"],"local_code_emission"=>$_POST["php_sucursal"],"terminal_code"=>$_POST["php_caja"],"ticket"=>$_POST["php_ticket"],"status"=>"ACTIVE"));
            if($model){

             $return="resultado=1";
             $model->status="INACTIVE";
             $model->save();
            }
            echo $return;
     }
 }
       public function actionPiece() {
        if (isset($_POST['php_idfb'])) {
             $return="resultado=0";
            $model= Piece::model()->findByAttributes(array("id_fb"=>$_POST['php_idfb']));
            if(!$model){
                $model= New Piece;
                $model->id_fb=$_POST['php_idfb'];
            }
            
            //$model->total_code=$_POST['php_totalcodigos'];
            $model->piece_1=$_POST['php_pieza1'];
            $model->piece_2=$_POST['php_pieza2'];
            $model->piece_3=$_POST['php_pieza3'];
            $model->piece_4=$_POST['php_pieza4'];
            $model->piece_5=$_POST['php_pieza5'];
            $model->piece_6=$_POST['php_pieza6'];
            if(isset($_POST['php_terminados'])){
            $model->finished=$_POST['php_terminados'];
            $model->piece_1=0;
            $model->piece_2=0;
            $model->piece_3=0;
            $model->piece_4=0;
            $model->piece_5=0;
            $model->piece_6=0;
            }
            if($model->save()){


              $return="resultado=1";
            }
            echo $return;
     }
 }
 public function actionCreateUser(){
     if (isset($_POST['php_idfb'])) {
        $return="resultado=0";
        $model= New UserProveedor;
        $model->id_fb=$_POST['php_idfb'];
        $model->name=$_POST['php_nombre'];
        $model->lastname=$_POST['php_apellido'];
        $model->email=$_POST['php_mail'];
        $model->province=$_POST['php_provincia'];
        $model->city=$_POST['php_ciudad'];
        $model->address=$_POST['php_direccion'];
        $model->phone=$_POST['php_telefono'];
        $model->identity=$_POST['php_cedula'];
        $model->proveedor=$_POST['php_proveedor'];
        $model->codigo_proveedor=$_POST['php_codproveedor'];
        $model->creation_date=date("Y-m-d");
        if($model->save()){
            $return="resultado=1";
        }
        echo $return;
     }
 }
 // public function actionCreate()
 //    {
 //        $model=new UserNavidad;
 //        $model2= new UserCubiertosTicket;

 //        // Uncomment the following line if AJAX validation is needed
 //        // $this->performAjaxValidation($model);

 //        if(isset($_POST['UserCubiertos']))
 //        {
            

 //            $user= UserCubiertos::model()->findbyPk($_POST['UserCubiertos']['identity']);
 //            if(!$user){
 //            $model->attributes=$_POST['UserCubiertos'];
 //            $model->creation_date=date('Y-m-d H:i:s');
 //            $model->save();
 //           }
 //             $model2->attributes=$_POST['UserCubiertosTicket'];
 //             $model2->identity=$_POST['UserCubiertos']['identity'];
 //            $model2->creation_date=date('Y-m-d H:i:s');
 //            if($model2->save()){
 //              $coupon= OpdCouponTransaction::model()->findByAttributes(array("coupon_number"=>$model2->coupon_number,"local_code_emission"=>$model2->local_code_emission,"terminal_code"=>$model2->terminal_code,"ticket"=>$model2->ticket,"status"=>"ACTIVE"));
 //              if($coupon){
 //                $coupon->status=="INACTIVE";
 //                $coupon->save();
 //              }
 //              $this->redirect(array("View","identity"=>$model->identity, 'title'=>'Felicidades'));
 //            }
 //        }

 //        $this->render('create',array(
 //            'model'=>$model,'model2'=>$model2
 //        ));
 //    }
   public function actionCreate()
    {
        $model=new UserArchidona;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['UserArchidona']))
        {
            
             
             echo var_dump($_POST['UserArchidona']);
             if ($_POST['UserArchidona']['birthdate'])
             {

               $fecha=$_POST['UserArchidona']['birthdate'];
               $fechanueva=substr($fecha,6,4).'-'.substr($fecha,3,2).'-'.substr($fecha,0,2);
               $_POST['UserArchidona']['birthdate']=$fechanueva;
               
               
               $_POST['UserArchidona']['birthdate']=$fecha;
             }
             $rnd = rand(0,9999);
             $model->attributes=$_POST['UserArchidona'];
             $model->image=CUploadedFile::getInstance($model,'image');
             $uploadedFile=CUploadedFile::getInstance($model,'image');
             $fileName = "{$rnd}-{$uploadedFile}";  
            
             $model->image = $fileName;
             echo $fileName;
             echo var_dump($model->image);


            if($model->save()){
                       $uploadedFile->saveAs('uploads/'.$fileName);
                      //$message = new YiiMailMessage;
                      //$message->view = 'message';//setea la vista que va a ser usada;
                      //$message->setSubject('Felicidades'.$model->name.' ya estas participando');//el subject del email;
                      //$message->setBody(array("model"=>$model), 'text/html');// manda el mensaje y el ciudadano a la vista mail/mensaje, la vista si debe tener algun formato o maquetado para que no llegue al spam.
                      //$front_name="tía actualizar";
                      //$message->setFrom(array("info@webcontent.ec"=>$front_name));
                      //$message->addTo($model->email);
                      //Yii::app()->mail->send($message);
                //$this->redirect(array('ticket','identity'=>$model->identity));
                       $this->redirect(array('finish'));
              }else{
                 $model->attributes=$_POST['UserArchidona'];
              }

        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }


  /*     public function actionCreate()
    {
        $model=new UserNavidad;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['UserNavidad']))
        {
            $model->attributes=$_POST['UserNavidad'];
            if($model->save()){
                      $message = new YiiMailMessage;
                      $message->view = 'message';//setea la vista que va a ser usada;
                      $message->setSubject('Felicidades'.$model->name.' ya estas participando');//el subject del email;
                      $message->setBody(array("model"=>$model), 'text/html');// manda el mensaje y el ciudadano a la vista mail/mensaje, la vista si debe tener algun formato o maquetado para que no llegue al spam.
                      $front_name="tía navidad";
                      $message->setFrom(array("info@webcontent.ec"=>$front_name));
                      $message->addTo($model->email);
                      Yii::app()->mail->send($message);
                $this->redirect(array('ticket','identity'=>$model->identity));
              }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }*/

        public function actionTicket($identity=null){
        $model= new UserArchidonaTicket;
             if(isset($_POST['UserArchidonaTicket']))
        {
        if($identity){
             $model->attributes=$_POST['UserArchidonaTicket'];
             $model->identity=$identity;

              
              //$errores = $coupon->model->getErrors();
              //var_dump($errores);
                 $coupon= OpdCouponTransaction::model()->findByAttributes(array("coupon_number"=>$model->coupon_number,"local_code_emission"=>$model->local_code_emission,"terminal_code"=>$model->terminal_code,"ticket"=>$model->ticket,"status"=>"ACTIVE"));
                if($coupon){
                  if($model->save()){
                  
                  if($coupon){
                    $coupon->status=="INACTIVE";
                    $coupon->save();
                  }
                  $this->redirect(array('finish'));
                }
                  
                }else{ 
                  Yii::app()->clientScript->registerScript('Error', 'alert("Este código no existe")');
                 // echo $model['coupon_number'];
                 // var_dump ($model->getErrors());
                   }

              
        }else{
             $model->attributes=$_POST['UserArchidonaTicket'];
             $model->identity=$_POST['UserArchidonaTicket']['identity'];

                     if($model->save()){
              $coupon= OpdCouponTransaction::model()->findByAttributes(array("coupon_number"=>$model->coupon_number,"local_code_emission"=>$model->local_code_emission,"terminal_code"=>$model->terminal_code,"ticket"=>$model->ticket,"status"=>"ACTIVE"));
              if($coupon){
                $coupon->status=="INACTIVE";
                $coupon->save();
              }
                 $this->redirect(array('finish'));
                }


        }
         }
           $this->render('ticket',array(
            'model'=>$model,
        ));
    }



/*
    public function actionTicket($identity=null){
        $model= new UserNavidadTicket;
             if(isset($_POST['UserNavidadTicket']))
        {
        if($identity){
             $model->attributes=$_POST['UserNavidadTicket'];
             $model->identity=$identity;
                     if($model->save()){
              $coupon= OpdCouponTransaction::model()->findByAttributes(array("coupon_number"=>$model->coupon_number,"local_code_emission"=>$model->local_code_emission,"terminal_code"=>$model->terminal_code,"ticket"=>$model->ticket,"status"=>"ACTIVE"));
              if($coupon){
                $coupon->status=="INACTIVE";
                $coupon->save();
              }
              $this->redirect(array('finish'));
                }
        }else{
             $model->attributes=$_POST['UserNavidadTicket'];
             $model->identity=$_POST['UserNavidadTicket']['identity'];

                     if($model->save()){
              $coupon= OpdCouponTransaction::model()->findByAttributes(array("coupon_number"=>$model->coupon_number,"local_code_emission"=>$model->local_code_emission,"terminal_code"=>$model->terminal_code,"ticket"=>$model->ticket,"status"=>"ACTIVE"));
              if($coupon){
                $coupon->status=="INACTIVE";
                $coupon->save();
              }
                 $this->redirect(array('finish'));
                }


        }
         }
           $this->render('ticket',array(
            'model'=>$model,
        ));
    }*/
        public function actionFinish(){
      $number=0;
      $tickets=date('H');
      $day=date('w');
      if($day!=0 || $day!=6){
        if($tickets<10){
            $number=0;
        }
        if($tickets>10 && $tickets < 14){
            $number=2;
        }
           if($tickets>14 && $tickets < 17){
            $number=6;
        }
                   if($tickets > 17){
            $number=11;
        }
      }
         $this->render('finish',array(
            'number'=>$number,
             ));
    }
    public function actionView($identity){
        $model=UserProveedor::model()->findbyAttributes(array('identity'=>$identity));
         $this->render('view',array(
            'model'=>$model,
        ));
    }

 public function actionConsult($php_idfb){
     if (isset($php_idfb)) {
        //$model= Piece::model()->findByAttributes(array("id_fb"=>$php_idfb));
        $user= UserProveedor::model()->findbyPk($php_idfb);
        $cad="";
        $exist=0;
        if($user){
            $exist=1;

        }
    header("Content-type:text/xml");
    echo chr(60).chr(63).'xml version="1.0" encoding="utf-8" '.chr(63).chr(62);

    
    
        
            //die(print_r ($t));
            
                            
    
                            
                echo '<datos>';
                if($user){
                $cad = "<existeUsuario>".$exist."</existeUsuario>"."<existePremio>".$user->codigo_proveedor."</existePremio>";
                }else{
                    $exist=0;
                 $cad = "<existeUsuario>".$exist."</existeUsuario>"."<existePremio>".$exist."</existePremio>";
                }
                echo $cad;
                echo '</datos>';
            
                
    }
    
   

     

 }
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}
