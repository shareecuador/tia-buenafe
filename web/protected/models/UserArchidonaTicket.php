<?php

/**
 * This is the model class for table "user_cubiertos_ticket".
 *
 * The followings are the available columns in table 'user_cubiertos_ticket':
 * @property string $identity
 * @property integer $ticket
 * @property string $coupon_number
 * @property integer $local_code_emission
 * @property integer $terminal_code
 * @property string $creation_date
 */
class UserArchidonaTicket extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_archidona_ticket';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('identity, ticket, coupon_number, local_code_emission, terminal_code', 'required'),
			array('ticket, local_code_emission, terminal_code', 'numerical', 'integerOnly'=>true),
			array('identity', 'length', 'max'=>10),
			array('coupon_number', 'unique'),
			array('ticket', 'unique'),
			array('coupon_number', 'length', 'max'=>27),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('identity, ticket, coupon_number, local_code_emission, terminal_code, creation_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'identity' => 'Cédula',
			'ticket' => 'Ticket',
			'coupon_number' => 'Cupón',
			'local_code_emission' => 'Sucursal',
			'terminal_code' => 'Caja',
			'creation_date' => 'Creation Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('identity',$this->identity,true);
		$criteria->compare('ticket',$this->ticket);
		$criteria->compare('coupon_number',$this->coupon_number,true);
		$criteria->compare('local_code_emission',$this->local_code_emission);
		$criteria->compare('terminal_code',$this->terminal_code);
		$criteria->compare('creation_date',$this->creation_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserCubiertosTicket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
