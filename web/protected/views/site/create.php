<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript" src="http://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>

<body id="background-rojo">
  <div id="cont-general" class="back-1">
    <div id="content">
      <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/buenafe-1.jpg" class="img-fondo">

    </div>
    <div id="txt-derecha">
      <div class="tit-rojo">¡Tía en Buena Fe <br/>Te premia</div>
      <div class="cont-black">      
          <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'user-cubiertos-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>false,'htmlOptions'=>array('enctype'=>'multipart/form-data'),
          )); ?>
          <?php //echo $form->errorSummary($model)."<br/>" ?>
                <div id="registro">
                   <?php echo $form->textField($model,'identity',array('size'=>10,'maxlength'=>10,'class'=>'identity onlynumbers','placeholder'=>'CÉDULA')); ?>
                 <?php echo $form->error($model,'identity'); ?>
                 <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100,'placeholder'=>'NOMBRE')); ?>
                 <?php echo $form->error($model,'name'); ?>
                  <?php echo $form->textField($model,'lastname',array('size'=>60,'maxlength'=>100,'placeholder'=>'APELLIDO')); ?>
                  <?php echo $form->error($model,'lastname'); ?>
                    <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255,'placeholder'=>'EMAIL')); ?>
              <?php echo $form->error($model,'email'); ?>
                  <?php echo $form->DropDownList($model,'province',array("Azuay",
              "Bolivar",
              "Cañar",
              "Carchi",
              "Chimborazo",
              "Cotopaxi",
              "El Oro",
              "Esmeraldas",
              "Galápagos",
              "Guayas",
              "Imbabura",
              "Loja",
              "Los Rios",
              "Manabí",
              "Morona Santiago",
              "Napo",
              "Orellana",
              "Pastaza",
              "Pichincha",
              "Santa Elena",
              "Santo Domingo de los Tsáchilas",
              "Sucumbíos",
              "Tungurahua",
              "Zamora Chinchipe")); ?>
              <?php echo $form->error($model,'province'); ?>
                  <?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>100,'placeholder'=>'CIUDAD')); ?>
              <?php echo $form->error($model,'city'); ?>
                  <?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255,'placeholder'=>'DIRECCIÓN')); ?>
              <?php echo $form->error($model,'address'); ?>
                  <?php echo $form->textField($model,'phone',array('size'=>20,'maxlength'=>20,'class'=>'onlynumbers','placeholder'=>'TELÉFONO')); ?>
              <?php echo $form->error($model,'phone'); ?>
                  <?php echo $form->textField($model,'cellphone',array('size'=>10,'maxlength'=>10,'class'=>'onlynumbers','placeholder'=>'CELULAR')); ?>
              <?php echo $form->error($model,'cellphone'); ?>
              <?php echo $form->textField($model,'birthdate',array('placeholder'=>'FECHA DE NACIMIENTO: 01-01-1990')); ?>
               <?php echo $form->error($model,'birthdate'); ?>
                <?php echo $form->DropDownList($model,'gender',array("Masculino"=>"Masculino", "Femenino"=>"Femenino"));?><br>
                
              <div class="cont-100">
                 <?php echo $form->checkBox($model,'terminos');?><label class="txt-terminos"><strong>Términos y Condiciones</strong></label>

                <?php echo $form->error($model,'terminos');   ?>
              </div>
              <input type="image" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/btn-siguiente.svg" class="id-siguiente"/>
  	 </div>
  <?php $this->endWidget(); ?>
   		
      </div>
      </div>
      <div class="banner-down">
         <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/banner-down.png"> 
      </div> 
    </div> 
       
  </div> 
</body>
<script type="text/javascript">
$(document).ready(function(){
  $('#UserArchidona_birthdate').mask('00-00-0000');
  $('#UserArchidona_phone').mask('0000000000');
  $('#UserArchidona_cellphone').mask('0000000000');
    $('#UserArchidona_identity').mask('0000000000');
  });
</script>
