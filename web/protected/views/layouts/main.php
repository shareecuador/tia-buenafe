<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
    <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/css/queries.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Tía Buena fe</title>
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
</head>

		<?= $content ?>

</html>
