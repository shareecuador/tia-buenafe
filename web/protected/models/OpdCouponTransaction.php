<?php

/**
 * This is the model class for table "opd_coupon_transaction".
 *
 * The followings are the available columns in table 'opd_coupon_transaction':
 * @property string $id
 * @property string $coupon_number
 * @property integer $local_code_emission
 * @property integer $terminal_code
 * @property integer $ticket
 */
class OpdCouponTransaction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'opd_coupon_transaction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('local_code_emission, terminal_code, ticket', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>20),
			array('coupon_number', 'length', 'max'=>27),
			array('status', 'length', 'max'=>8),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, coupon_number, local_code_emission, terminal_code, ticket', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'coupon_number' => 'Coupon Number',
			'local_code_emission' => 'Local Code Emission',
			'terminal_code' => 'Terminal Code',
			'ticket' => 'Ticket',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('coupon_number',$this->coupon_number,true);
		$criteria->compare('local_code_emission',$this->local_code_emission);
		$criteria->compare('terminal_code',$this->terminal_code);
		$criteria->compare('ticket',$this->ticket);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OpdCouponTransaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
