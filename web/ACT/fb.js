(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
window.fbAsyncInit = function() {
    FB.init({
        appId: '983887574960348',
        cookie: true, // enable cookies to allow the server to access 
        // the session
        xfbml: true, // parse social plugins on this page
        version: 'v2.1' // use version 2.1
    });
};

function fb_login() {
	FB.getLoginStatus(function(response) {
  		if (response.status === 'connected') {
  			FB.api('/me', function(response) {
		        if (response && !response.error) {
		            console.log(response);
		            window.location = 'regalo.html';
		        }
		    });
  		}
  		else{
		    FB.login(function(response) {
		        if (response.authResponse) {
		            console.log('Welcome!  Fetching your information.... ');
		            //console.log(response); // dump complete info
		            access_token = response.authResponse.accessToken; //get access token
		            user_id = response.authResponse.userID; //get FB UID
		            FB.api('/me', function(response) {
				        if (response && !response.error) {
				            console.log(response);
				            window.location = 'regalo.html';
				        }
				    });

		        } else {
		            //user hit cancel button
		            console.log('User cancelled login or did not fully authorize.');

		        }
		    }, {
		        scope: 'email'
		    });
		}
	});
}

function fetch() {
    FB.api('/me', function(response) {
        if (response && !response.error) {
            return response;
        }
    });
}

function share(comentario, idfb) {
    var rand2 = Math.random();
    var width = 626;
    var height = 436;
    var leftPosition, topPosition;
    //Allow for borders.
    leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
    //Allow for title and status bars.
    topPosition = (window.screen.height / 2) - ((height / 2) + 50);
    var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
    u = 'https://www.nissanecuador.webcontent.ec/aplicaciones/fb/nissan_regalos/redirect.html?v='+rand2;
    t = document.title;
    window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', windowFeatures);
    return false;
}
