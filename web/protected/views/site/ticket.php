<body id="background-rojo">
	<div id="cont-general" class="back-2">
        <div class="content">
          <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/buenafe-2.jpg" id="img-codigo" class="img-fondo">

          <div id="txt-derecha">
            <div class="tit-rojo text-right">INGRESA <br/>TU CÓDIGO</div>
            <div class="cont-black">  
                <?php $form=$this->beginWidget('CActiveForm', array(
                  'id'=>'user-cubiertos-form',
                  // Please note: When you enable ajax validation, make sure the corresponding
                  // controller action is handling ajax validation correctly.
                  // There is a call to performAjaxValidation() commented in generated controller code.
                  // See class documentation of CActiveForm for details on this.
                  'enableAjaxValidation'=>false,
                )); ?>
                <div id="registro">
                	        <?php if(!isset($_GET['identity'])){ ?>
                	        <?php echo $form->textField($model,'identity',array('size'=>10,'maxlength'=>10,'class'=>'onlynumbers','placeholder'=>'CÉDULA')); ?>
   							 <?php echo $form->error($model,'identity'); ?>
   							 <?php } ?>
                  <?= $form->textField($model,'coupon_number',array('size'=>100,'maxlength'=>100,'class'=>'onlynumbers','placeholder'=>'CÓDIGO')); ?>
                          <?= $form->error($model,'coupon_number'); ?>
                        	<?= $form->textField($model,'local_code_emission',array('size'=>100,'maxlength'=>100,'class'=>'onlynumbers','placeholder'=>'SUCURSAL')); ?>
                        	<?= $form->error($model,'local_code_emission'); ?>
        	            	<?= $form->textField($model,'terminal_code',array('size'=>100,'maxlength'=>100,'class'=>'onlynumbers','placeholder'=>'CAJA')); ?>
                        	<?= $form->error($model,'terminal_code'); ?>
        	            	<?= $form->textField($model,'ticket',array('size'=>100,'maxlength'=>100,'class'=>'onlynumbers','placeholder'=>'TICKET')); ?>
                        	<?= $form->error($model,'ticket'); ?>
            			
                         <input type="image" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/btn-siguiente.svg" class="id-siguiente margin-top-a"/>
                </div>
<?php $this->endWidget(); ?>
            </div>

        </div>
        <div class="banner-down">
            <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/banner-down.png"> 
        </div> 
     </div>
</body>