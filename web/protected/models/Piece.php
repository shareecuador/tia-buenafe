<?php

/**
 * This is the model class for table "piece".
 *
 * The followings are the available columns in table 'piece':
 * @property integer $id
 * @property string $id_fb
 * @property string $total_code
 * @property string $piece_1
 * @property string $piece_2
 * @property string $piece_3
 * @property string $piece_4
 * @property string $piece_5
 * @property string $piece_6
 * @property string $finished
 */
class Piece extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'piece';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_fb, piece_1, piece_2, piece_3, piece_4, piece_5, piece_6', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('id_fb', 'length', 'max'=>100),
			array('total_code, piece_1, piece_2, piece_3, piece_4, piece_5, piece_6', 'length', 'max'=>1),
			array('finished', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_fb, total_code, piece_1, piece_2, piece_3, piece_4, piece_5, piece_6, finished', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_fb' => 'Id Fb',
			'total_code' => 'Total Code',
			'piece_1' => 'Piece 1',
			'piece_2' => 'Piece 2',
			'piece_3' => 'Piece 3',
			'piece_4' => 'Piece 4',
			'piece_5' => 'Piece 5',
			'piece_6' => 'Piece 6',
			'finished' => 'Finished',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_fb',$this->id_fb,true);
		$criteria->compare('total_code',$this->total_code,true);
		$criteria->compare('piece_1',$this->piece_1,true);
		$criteria->compare('piece_2',$this->piece_2,true);
		$criteria->compare('piece_3',$this->piece_3,true);
		$criteria->compare('piece_4',$this->piece_4,true);
		$criteria->compare('piece_5',$this->piece_5,true);
		$criteria->compare('piece_6',$this->piece_6,true);
		$criteria->compare('finished',$this->finished,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Piece the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
